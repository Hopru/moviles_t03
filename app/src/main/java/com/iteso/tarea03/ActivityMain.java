package com.iteso.tarea03;

import android.os.PersistableBundle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;
import android.support.design.widget.Snackbar;

public class ActivityMain extends AppCompatActivity {

    RadioGroup colors;
    Button small;
    Button medium;
    Button large;
    Button xlarge;
    boolean isAddedToCart;
    Button addToCart;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        colors = findViewById(R.id.am_rg_color);
        small = findViewById(R.id.am_b_small);
        medium = findViewById(R.id.am_b_medium);
        large = findViewById(R.id.am_b_large);
        xlarge = findViewById(R.id.am_b_xlarge);
        addToCart = findViewById(R.id.am_b_add_to_cart);

        small.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent event) {
             return   setPressedState(small, view, event);
            }
        });
        medium.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent event) {
             return  setPressedState(medium, view, event);
            }
        });
        large.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent event) {
               return setPressedState(large, view, event);
            }
        });
        xlarge.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent event) {
                return setPressedState(xlarge, view, event);
            }
        });

    }

    public boolean setPressedState(Button button, View view, MotionEvent event) {
        // show interest in events resulting from ACTION_DOWN
        if (event.getAction() == MotionEvent.ACTION_DOWN) return true;

        // don't handle event unless its ACTION_UP so "doSomething()" only runs once.
        if (event.getAction() != MotionEvent.ACTION_UP) return false;

        switch(view.getId()) {
            case R.id.am_b_small:
                small.setPressed(true);
                medium.setPressed(false);
                large.setPressed(false);
                xlarge.setPressed(false);
                break;
            case R.id.am_b_medium:
                small.setPressed(false);
                medium.setPressed(true);
                large.setPressed(false);
                xlarge.setPressed(false);
                break;
            case R.id.am_b_large:
                small.setPressed(false);
                medium.setPressed(false);
                large.setPressed(true);
                xlarge.setPressed(false);
                break;
            case R.id.am_b_xlarge:
                small.setPressed(false);
                medium.setPressed(false);
                large.setPressed(false);
                xlarge.setPressed(true);
                break;
            default:
                break;
        }
        return true;
    }
    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        restore(savedInstanceState);
        super.onRestoreInstanceState(savedInstanceState);
    }

    public void restore(Bundle savedInstance) {
        colors.check(savedInstance.getInt("COLORS"));
        small.setPressed(savedInstance.getBoolean("SMALL"));
        medium.setPressed(savedInstance.getBoolean("MEDIUM"));
        large.setPressed(savedInstance.getBoolean("LARGE"));
        xlarge.setPressed(savedInstance.getBoolean("XLARGE"));
        addToCart.setText(savedInstance.getString("ADD_TO_CART"));
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putInt("COLORS", colors.getCheckedRadioButtonId());
        outState.putBoolean("SMALL", small.isPressed());
        outState.putBoolean("MEDIUM", medium.isPressed());
        outState.putBoolean("LARGE", large.isPressed());
        outState.putBoolean("XLARGE", xlarge.isPressed());
        outState.putString("ADD_TO_CART", (String) addToCart.getText());
        super.onSaveInstanceState(outState);
    }

    public void onLikeClick(View view) {
        LayoutInflater inflater = this.getLayoutInflater();
        View layout = inflater.inflate(R.layout.activity_main_toast, (ViewGroup) ActivityMain.this.findViewById(R.id.activity_main_save_toast_text));

        TextView textView = layout.findViewById(R.id.activity_main_save_toast_text);

        textView.setText("+1 to Vintage Bicycle");

        Toast custom = new Toast(ActivityMain.this);
        custom.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
        custom.setDuration(Toast.LENGTH_SHORT);
        custom.setView(layout);
        custom.show();
    }

    public void onAddToCartClick(View view) {
        addToCartClick();
        Snackbar bar = Snackbar.make(view, "Added to Cart", Snackbar.LENGTH_INDEFINITE)
                .setAction("UNDO", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // Handle user action
                        addToCartClick();
                    }
                });

        bar.show();
    }

    public void addToCartClick() {
        if(isAddedToCart) {
            addToCart.setText(R.string.button_add_to_cart);
        } else {
            addToCart.setText(R.string.button_added_to_cart);
        }
        isAddedToCart = !isAddedToCart;
    }
}
